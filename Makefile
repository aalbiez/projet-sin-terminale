
all: rapport

rapport:
	@latexmk -f $<

clean:
	@latexmk -C -silent

docker-pdf: docker-pull
	docker run --rm -it -v $(shell pwd):/data registry.gitlab.com/azae/outils/latex/cli make rapport

docker-pull:
	docker pull registry.gitlab.com/azae/outils/latex/cli
