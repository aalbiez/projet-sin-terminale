[![pipeline status](https://gitlab.com/aalbiez/projet-sin-terminale/badges/master/pipeline.svg)](https://gitlab.com/aalbiez/projet-sin-terminale/commits/master)

# Rapport de projet SIN de terminale

Document sous license [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).


[Le rapport](https://gitlab.com/aalbiez/projet-sin-terminale/builds/artifacts/master/file/rapport.pdf?job=latex)
